package fr.afpa.demojwt.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.demojwt.entities.Authentification;
import fr.afpa.demojwt.jwt.JwtUtils;



@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class RestControllerAuthentification {

	@PostMapping(value = "/authentification")
	public ResponseEntity<?> authentifPost(@RequestBody Authentification authentification ) {

		HttpStatus status;
		 String token=null;
		 Map<String, String> reponse  = new HashMap<String, String>(); 

		// verification de l'authentification
		boolean verifAuthentification  = "Fayak".equals(authentification.getLogin()) && "1234".equals(authentification.getMdp()); 
		
		if (verifAuthentification) { 
			//generation du token
			token = JwtUtils.createJWT("BackDemoJWT","front",authentification.getLogin(),"admin",10000000000L);
			reponse.put("token", token);
			 status =HttpStatus.OK;
		} else { 
			status=HttpStatus.BAD_REQUEST;
		}
		return (ResponseEntity<?>) ResponseEntity.status(status).body(reponse);
	}
	
	
	@GetMapping(value = "/pageprotected")
	public ResponseEntity<?> getPageProtected(@RequestHeader HttpHeaders header) {
		
		HttpStatus status;
		 Map<String, String> reponse  = new HashMap<String, String>(); 
				
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		
		if (headerAutorisation.isPresent()) {
			
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), "admin")) {
				status = HttpStatus.OK;
				reponse.put("message","Vous etes authentifiť");
				return ResponseEntity.status(status).body(reponse);
			}
		}
		
		reponse.put("message","Vous n'etes pas authentifiť");
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body(reponse);

	}
		
	
}



