import React, { useState } from 'react';


export default function Login() {

  const [authentification, setAuthentification] = useState({});
  const [erreur, setErreur] = useState("");

  async function submitForm(event) {
    event.preventDefault();
    let response = await fetch(`${process.env.REACT_APP_API_URL}/authentification`, {
      method: "POST",
      body: JSON.stringify(authentification),
      headers: {
        'Accept': 'application/json',
        "Content-type": "application/json; charset=UTF-8"
      }
    })
      .catch(error => console.log(error));
      sessionStorage.clear()

      if (response && response.status===200) {
      let token = await response.json()
      sessionStorage.setItem('token',token.token);
      document.location.href = "/pageprotected";
    } else{
      if(response){
      setErreur("Erreur login ou mot de passe incorrects !")
    }else{
      setErreur("serveur non disponible !")
    }
    }
  }

  function changeHandler(event) {
    let authentificationTemp = authentification
    authentificationTemp[event.target.name] = event.target.value;
    setAuthentification(authentification)
  };

  return (
    <div className="tst">
      <div className="container-fluid">
        <div className="row no-gutter">
          <div className="col-md-8 col-lg-6">
            <div className="hauteurConteneur d-flex align-items-center py-5">
              <div className="container">
                <div className="row">
                  <div className="col-md-9 col-lg-8 mx-auto">


                    <form onSubmit={submitForm}>
                      <h3 className="TitreLogin mb-4">Connexion</h3>

                      <div className="groupe-login ">
                        <label htmlFor="login" className="labelLogin">Login</label>
                        <div className="input-group">
                          <i class="userIcone far fa-user"/>
                          <input type="text" id="login"
                            className="inputConnect form-control"
                            placeholder="Login"
                            name="login"
                            success="right"
                            onChange={changeHandler}
                            required autoFocus />
                        </div>
                      </div>

                      <div className="groupe-login ">
                        <label htmlFor="inputPassword" className="labelLogin">Mot de Passe</label>
                        <div className="input-group">
                          <i class="cadenas fas fa-lock"/>
                          <input type="password"
                            id="inputPassword"
                            className="form-control"
                            placeholder="Password"
                            name="mdp"
                            onChange={changeHandler}
                            required />
                        </div>

                      </div>
                      <p className="errorLog text-center mb-4 red-text" >{erreur}</p>
                      <button id="btnLogin" className="btn btn-lg  btn-block btn-login text-uppercase font-weight-bold mb-2"
                        type="submit">Connexion</button>
                      <div className="text-center">
                        <a className="small" href="/admin">Connexion partie administrateur</a>
                      </div>
                    </form>
                  </div>
                </div>
                <div className="row">

                </div>
              </div>
            </div>
          </div>
          <div className="hauteurConteneur d-none d-md-flex col-md-4 col-lg-6 bg-image">

          </div>

        </div>
      </div>
    </div>
  );

}