import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,Redirect,
} from "react-router-dom";

import Login from './Components/Login'
import PageProtected from './Components/PageProtege'
import { ToastProvider } from 'react-toast-notifications'
import './App.css';


import 'mdbreact/dist/css/mdb.css';



function App() {
  return (
    <ToastProvider>
    <Router>     
       <Switch>
         <PrivateRoute path="/protectedpage">
           <PageProtected />
         </PrivateRoute>

         <Route path="/" >
           <Login />
         </Route>

       </Switch>
   </Router>
   </ToastProvider>
  );


  function PrivateRoute({ children, ...rest }) {
    return (
      <Route
        {...rest}
        render={({ location }) =>
          sessionStorage.token  ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/",
                state: { from: location }
              }}
            />
          )
        }
      />
    );
  }

 
}

export default App;
