import React,{useState, useEffect} from 'react';


export default function PageProtege(){

    const [messageServeur, setMessageServeur] = useState([]);

 const getMessageServeur =  async () => {
        let response = await fetch(`${process.env.REACT_APP_API_URL}/pageprotected`, {
            method: "GET",
            headers: {
                'Authorization': sessionStorage.token
            }
        }).catch(error => {console.log(error);}); 


            if (response && response.status===200) {

                let responseData = await response.json();
                setMessageServeur(responseData);

            }else if(response && response.status===401){

                let responseData = await response.json();
                setMessageServeur(responseData);

               } 
    }

    useEffect(() => {
        getMessageServeur();
    },[])
  
    return <div>
        page protégée
        <br/>  <br/>
        Message envoyé par le back : {messageServeur.message}
  
    </div>
}